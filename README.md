**安装依赖包**
```
npm i
```

**开发**
```
npm start
```

**打包**
```
npm run uat
```

**开发注意事项**
1. 每次添加自定义svg图标时，直接加到public/icons目录下, 然后执行以下命令精简svg
* 若本地未安装 svgo, 先安装：
  ```
  npm i svgo -g
  ```
* 精简整个icons文件:
  ```
  svgo -f public/icons
  ```
* 精简单独的svg:
  ```
  svgo public/icons/xxx.svg
  ```