#!/bin/bash 

set -x

cd build
for d in `ls `
do
	cd $d
	zip -r ../$d.zip ./*
	cd ..
	rm -rf $d
    mv $d.zip ../build-pkg/$d.zip
done


# 解压
# tar -zxvf file.tar.gz

# zip 压缩
# cd 相应文件夹
# zip -r cpad.zip ./*
