import jsonp from 'jsonp';
import { Toast } from 'antd-mobile';
import axios from './index';

/**
 * 获取累计使用人数
 */
const getUserCount = () => {
    return axios.get(`/api/digitalhuman/aitrainer/count/${window.AID}`)
        .then(res => res)
        .catch(err => Toast.fail(err.message || '人数统计失败'));
};

/**
 * 获取图形验证码
 */
const getValidatecode = () => {
  console.log('17531753175317531753');
    return axios.get('/api/digitalhuman/aitrainer/getImageCode').then(res => res);
};

/**
 * 本地开发登录, 不含验证码
 */
const devLogin = param => {
    return axios.post('/api/digitalhuman/aitrainer/login', param).then(res => res);
};

/**
 * 测试及生产环境登录, 含验证码
 */
const login = param => {
    return axios.post('/api/digitalhuman/aitrainer/loginWithCode', param).then(res => res);
};

/**
 * 获取浦发银行公众号微信权限配置参数
 */
const getPufaWXConfig = () => {
    const url = window.location.href.split('#')[0];
    const requestUrl = `/api/digitalhuman/aitrainer/wx/getSignature?url=${encodeURIComponent(url)}`;
    return axios.get(requestUrl)
        .then(res => {
            if (res) {
                return res;
            }
            throw Error();
        });
};
/**
 * 获取安络杰公众号微信权限配置参数
 */
const getAnluojieWXConfig = () => {
    const url = window.location.href.split('#')[0];

    let requestUrl = 'https://validation.energytrust.com.cn/v1/weixin/jssdk';
    requestUrl += `?url=${encodeURIComponent(url)}`;
    requestUrl += '&api=startRecord,stopRecord,onVoiceRecordEnd,uploadVoice,updateAppMessageShareData,';
    requestUrl += '&debug=true';

    return new Promise((resolve, reject) => {
        jsonp(
            requestUrl,
            { timeout: 30 * 1000 },
            (err, data) => {
                console.log('jsonp callback: ', data);
                if (data) {
                    resolve(data);
                } else {
                    reject(err);
                }
            }
        );
    });
};

const getWxCode = (appid) => {
    let requestUrl = 'https://open.weixin.qq.com/connect/oauth2/authorize';
    let url = window.location.href;
    requestUrl += `?appid=${appid}`;
    requestUrl += `&redirect_uri=${encodeURIComponent(url)}`;
    requestUrl += '&response_type=code&scope=snsapi_base&state=123#wechat_redirect';

    console.log("requestUrl",requestUrl)

    return new Promise((resolve, reject) => {
        jsonp(
            requestUrl,
            { timeout: 30 * 1000 },
            (err, data) => {
                console.log('jsonp callback: ', data);
                if (data) {
                    resolve(data);
                } else {
                    reject(err);
                }
            }
        );
    });
}

const getOpenId = param => {
    return axios.get('/api/digitalhuman/aitrainer/wx/getOpenId', param).then(res => res);
};

//图形验证码
const getImgcode = () => {
    return axios.get("/api/digitalhuman/aitrainer/getImageCode")
    .then(resData => {
      return resData;
    })
    .catch(err => {
      Toast.fail(err ? err.data : "请求失败");
      return null;
    });
  }
  //校验验证码
  const checkCode = params => {
    return axios.post("/api/digitalhuman/aitrainer/checkCode",params)
    .then(resData => {
      return resData;
    })
    .catch(err => {
      Toast.fail(err ? err.data : "请求失败");
      return null;
    });
  };

/**
 *  获取理财经理信息
 * @param employeeId
 * @returns {Promise<AxiosResponse<T>>}
 */
const getPcmInfo = employeeId => {
  return axios.get(`/api/digitalhuman/aitrainer/getEmployee/${employeeId}`).then(res => res);
};

/**
 * 获取每周市场详情
 * @param param json eg:{'currentDate':'2020-05-25'}
 * @returns {Promise<AxiosResponse<T>>}
 */
const getMarketInfo = param =>{
  return axios.post('/api/digitalhuman/aitrainer/proxy/aitrainerdatas/marketinfo/getMarketInfo',param).then(res => res);
};

/**
 * 获取每周重点产品基金/信托/资管
 * @param param json eg:{date:'2020-05-25',productType: '基金/信托/资管'}
 * @returns {Promise<AxiosResponse<T>>}
 */
const getProductInfo = param =>{
  return axios.post('/api/digitalhuman/aitrainer/proxy/aitrainerdatas/productinfo/getProductInfo',param).then(res =>res);
};

/**
 * 获取每周重点产品:公募基金/信托级资管
 * @param param json eg:{week:'2020-05-25',mainType: '公募基金'}
 * @returns {PromiseLike<any> | Promise<any>}
 */
const getProductByType = param =>{
  return axios.get('api/digitalhuman/aitrainer/proxy/aitrainerdatas/productinfo/getProductByType',param).then(res => res);
};

/**
 * 获取产品信息
 * @returns {PromiseLike<any> | Promise<any>}
 * @param productId
 */
const findProductById = productId =>{
  return axios.get(`api/digitalhuman/aitrainer/proxy/aitrainerdatas/productinfo/findProductById/${productId}`).then(res => res);
};

const getProductById = param =>{
  return axios.get(`api/digitalhuman/aitrainer/proxy/aitrainerdatas/productinfo/getProductById/`,param).then(res => res);
};
/**
 * 获取每天发售产品
 * @param param
 */
const getProductInfoByScaleTime = param =>{
  return axios.get('api/digitalhuman/aitrainer/proxy/aitrainerdatas/productinfo/findProductByScale',param).then(res => res);
};

/**
 * 获取金字塔数据
 * @returns {PromiseLike<any> | Promise<any>}
 */
const getPyramidConfigs = () =>{
  return axios.get('api/digitalhuman/aitrainer/proxy/aitrainerdatas/pyramidconfig/getPyramidConfigs').then(res => res);
};

const getToken = (empId) => {
  return axios.get(`/api/digitalhuman/aitrainer/getToken/${empId}`)
}

const getStrategy = () => {
  return axios.get(`/api/digitalhuman/aitrainer/proxy/aitrainerdatas/marketstrategy/getStrategy`)
}

const getTimes = (empId) => {
  return axios.get(`/api/digitalhuman/aitrainer/proxy/aitrainerdatas/exCourse/getTimes/${empId}`)
}


export default {
  getUserCount,
  getValidatecode,
  login,
  devLogin,
  getPufaWXConfig,
  getAnluojieWXConfig,
  getWxCode,
  getOpenId,
  getImgcode,
  checkCode,
  getPcmInfo,
  getMarketInfo,
  getProductInfo,
  getProductByType,
  getProductById,
  getPyramidConfigs,
  getProductInfoByScaleTime,
  getToken,
  getStrategy,
  findProductById,
  getTimes
};
