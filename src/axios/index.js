import axios from 'axios';

import { getBaseUrl } from 'EnvConfig';
import getErrorCode from './errorCode';

const networkhealth=()=>{
    window.wx.getNetworkType({
      success: function (res) {
        // alert(res.networkType);
      },
      fail: function (res) {
        alert('网络已断开');
      }
    });
}

// 过滤请求
axios.interceptors.request.use(
    config => {
        const token = sessionStorage.getItem('token');
        config.headers.Authorization = `${token}`;
        config.baseURL = getBaseUrl();
        config.timeout = 20 * 1000;
        return config;
        
    },
    error => {
        return Promise.reject(error);
    }
);

// 添加响应拦截器
axios.interceptors.response.use(
    response => {
        console.log('1805180518051805');
        // networkhealth();
        console.log('axios response:', response);
        const data = response.data;
        if (data) {
            return Promise.resolve(data);
        }
        return Promise.reject();
    },
    error => {
        console.log('axios error:', error);
        networkhealth();
        const response = error && error.response;
        // if(response.status === 401){
        //     return error.response
        // }
        if (response) {
            if (response.data && typeof response.data === 'string') {
                // return Promise.reject(Error(response.data));
                return Promise.reject(response.data)
            }
            const resMsg = getErrorCode(response);
            const msg = `${resMsg} (${response.status})`;
            return Promise.reject(msg);
        }

        

        const returnError = error.message
            ? error.message.includes('timeout')
                ? Error('接口调用超时')
                : error
            : Error('请求失败');
        console.log('returnError: ', returnError);
        return Promise.reject(returnError);
    }
);

export default {
    post: (url, data) => axios.post(url, data),
    put: (url, data) => axios.post(url, data),
    get: (url, data) => axios.get(url, { params: data }),
    delete: (url, data) => axios.delete(url, { params: data })
};
