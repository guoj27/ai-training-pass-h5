import React, {useEffect, useRef, useState} from 'react';
import './index.scss';
import { getUrlParam } from 'Utils';
import api from '../../../axios/api';
import gifPoster from 'Imgs/video-poster.gif';
import pngPoster from 'Imgs/ex.png';

let replacePng = null;

export default props => {
  const[btnSrc, setBtnSrc] = useState("time-out");
  const [video, setVideo] = useState();
  const [pcmInfo,setPcmInfo] = useState({});
  const [strategy, setStrategy] = useState();
  const [poster, setPoster] = useState();
  const [videoUrl, setVideoUrl] = useState();
  const [week, setWeek] = useState();
  
  const videoRef = useRef(null);
  useEffect(()=>{
    api.getToken(getUrlParam().empId).then(res => sessionStorage.setItem("token", res));

    api.getPcmInfo(getUrlParam().empId).then(res => setPcmInfo(res));

    api.getStrategy().then(res => {
      setStrategy(res);
      setWeek(res.week);

      setVideoUrl(`https://spdb.cdn.bcebos.com/WeeklyMarketInfo/${res.week}/Managers/default_manager.mp4`)
      // setVideoUrl(`https://spdb.cdn.bcebos.com/WeeklyMarketInfo/${res.week}/Managers/${getUrlParam().empId}_manager.mp4`);
      var ImgObj=new Image();
      ImgObj.src = `https://spdb.cdn.bcebos.com/WeeklyMarketInfo/${res.week}/Managers/${getUrlParam().empId}_manager.png`;
      if(ImgObj.fileSize > 0 || (ImgObj.width > 0 && ImgObj.height > 0))
      {
        setPoster(ImgObj.src);
      } else {
        setPoster(pngPoster);
      }

    });

    replacePng = setTimeout(() => {
      document.getElementsByClassName("play-btn")[0].classList.remove("active-btn");
    },12000)
  },[]);

  useEffect(() => {
    videoRef.current && setVideo(videoRef.current);
  },[videoRef])

  const onPlayBtnClick = () => {
    clearTimeout(replacePng)
    document.getElementsByClassName("play-btn")[0].classList.remove("active-btn");

    const videoDom = document.getElementById("intro-video");
    let timer = parseInt(videoDom.duration) - parseInt(videoDom.currentTime);
  
    setTimeout(() => {
      document.getElementsByClassName("video-bg")[0].style.backgroundImage = `url(${loadMedia(strategy.marketEmotion)})`;
    }, 3000);

    if(videoDom.paused) {
      setBtnSrc("play");
      video && video.play();
    }else {
      setBtnSrc("time-out");
      video && video.pause();
    }
  };

  const onVideoPlay = () => {
    setBtnSrc("play");
    video && video.play();
  }

  const onVideoEnded = () => {
    setBtnSrc("time-out");
    video && video.pause();
    document.getElementsByClassName("video-bg")[0].style.backgroundImage = 'url("none")';
  }

  const onVideoPlayError = () => {
    console.error('video play error');
    onVideoEnded();

    
  }

  const loadMedia = (emotionUrl) => {
    const resource = require('Imgs/emotion/'+emotionUrl+'.png');
    return resource;
  }

  return (
    <div className="intro-card">
        <div className="video">
          <div className="video-bg" >
            <video
              className="intro-video"
              id="intro-video"
              ref={videoRef}
              src={videoUrl}
              onPlay={onVideoPlay}
              onEnded={onVideoEnded}
              preload="auto"
              onError={onVideoPlayError}
              playsInline
              webkit-playsinline="true"
              x5-video-player-fullscreen="false"
              x5-video-player-type="h5"
              poster={poster}
            />
          </div>
          
          <div className="play-btn active-btn" onClick={onPlayBtnClick}>
            <img src={require(`Imgs/${btnSrc}.png`)}/>
            <div className="play-text">{btnSrc === "play" ? "暂停播放" : "播放简报"}</div>
          </div>
        </div>
        <div className="intro-text">
          <div className="fina-name">{pcmInfo.employeeName}</div>
          <div className="fina-item">理财经理 工号：{pcmInfo.employeeId}</div>
          <div className="fina-item">{pcmInfo.orgName}</div>
          <div className="fina-item">23年金融从业经验，资深理财经理</div>
        </div>
    </div>
  )
}