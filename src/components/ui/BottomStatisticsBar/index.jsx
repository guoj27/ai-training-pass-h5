import React, {useRef, useState, useEffect} from 'react';
import '../../../../public/config';
import './index.scss';
import api from '../../../axios/api';

export default props => {
    const [timer, setTimer] = useState(null);
    useEffect(() => {
        api.getTimes(sessionStorage.getItem('employeeId'))
        .then(res=> {
            setTimer(res)
        })
    },[])
    
    const passUrl = () => {
        window.location = 'https://x.spdb.com.cn/fundation/AIPassH5/#/Entry?empId='+sessionStorage.getItem('employeeId');
    }
    return (
        <div className="bottomstatisticsbar">
                <div className="count-text">
                    共计完成 <span className="number">{timer}</span> 次对练
                </div>
                <div className="buttoninbar" src={require('../../../../public/imgs/jump.png')}  onClick={passUrl}></div>
        </div>
    );
};































