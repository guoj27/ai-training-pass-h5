import React, { useCallback } from 'react';

import { CustomIcon } from 'Components/ui';
import './index.scss';

export default props => {
    const defautHandler = useCallback(() => {}, []);

    const iconEle = props.icon && (
        <span className="btn-icon">
            <CustomIcon type={props.icon} />
        </span>
    );

    return props.type === 'outline'
        ? <div className='da-outline-btn' onClick={props.onClick || defautHandler}>
            <div className="btn-content">
                {props.children}
                {iconEle}
            </div>
        </div>
        : <div className='da-btn light-text' onClick={props.onClick || defautHandler}>
            <div className="btn-content">
                {props.children}
                {iconEle}
            </div>
        </div>;
};

// 调用
/* <Button type="outline">测试</Button> */
