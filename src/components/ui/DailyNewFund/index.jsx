import React from 'react';
import '../../../../public/config';
import './index.scss';
import ShareButton from '../ShareButton/index'

export default props => {
    const { curWeekDay, shareClick, proType} =props;

    const dateArr = [{'Mo':'周一'}, {'Tu':'周二'}, {'We':'周三'},{'Th': '周四'}, {'Fr':'周五'}, {'Sa':'周六'},{'Su':'周日'},{'all':'全部'}];
    return (
        <div className="dailynewfund">
            <div className="dailynewfundtitle">
                <div className="dailynewfundtitledate">
                    {/* <div className="dailynewfundtitledateen">{dayen}</div>
                    <div className="dailynewfundtitledatech">{daych}</div> */}
                    <div className="dailynewfundtitledateen">{Object.keys(dateArr[curWeekDay])}</div>
                    <div className="dailynewfundtitledatech">{Object.values(dateArr[curWeekDay])}</div>
                </div>
                <div className="dailynewfundtitleword">新发{proType}</div>
                <div className="dailynewfundtitlebutton">
                    <ShareButton sharetitle={"发给客户"} shareClick={shareClick} {...props}></ShareButton>
                </div>   
            </div>
            <div className="dailynewfundboard">
                { props.children }
            </div>
        </div>
    );
};