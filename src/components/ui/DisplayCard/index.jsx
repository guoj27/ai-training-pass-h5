import React from 'react';
import '../../../../public/config';
import './index.scss';
import ShareButton from '../ShareButton/index';
import { getUrlParam } from 'Utils';

export default props => {
    const {title,displaythumb,buttoncontent,audiotime,bottomcardheight,displaybutton }=props;
    return (
        <div className="entirecard">
            <div className="topcard">
                {
                !getUrlParam().empId ?
                <div className="buttonmodule" >
                    <ShareButton sharetitle="发给客户" {...props}></ShareButton>
                </div>
                : 
                <div className="buttonmodule"></div>
                }
                <div className="title">
                    {/* {displaythumb ?<img className="picture1" src={require('../../../../public/imgs/recommend.png')}></img>:null} */}
                    {/* <span className="word">{title}</span> */}
                    {/* <img src={require('../../../../public/imgs/recommend-white.png')}></img> */}
                    <span className="word">{title}</span>
                </div>
                <div className="picture2div">
                    <img className="picture2" src={require('../../../../public/imgs/topcardxiaopu.png')}></img>
                </div>
            </div>
            <div className="bottomcard" style={{height:bottomcardheight}}>
                { props.children }
            </div>
        </div>
    );
};