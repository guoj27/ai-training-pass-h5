import React from 'react';
import { Icon } from 'antd-mobile';

import './index.scss';

export default props => {
    const { description } = props;
    return (
        <div className="page-loading-container">
            <div className="center">
                <div>
                    <Icon type="loading" size="lg" />
                </div>
                {description && (
                    <div className="description">
                        {description}
                    </div>
                )}
            </div>
        </div>
    );
};
