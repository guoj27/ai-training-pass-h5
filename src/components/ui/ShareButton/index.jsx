import React from 'react';
import './index.scss';

export default props => {
  const { sharetitle, shareClick } = props;

  const share = () => {
    shareClick();
  };

  return (
    <div className="share-btn" onClick={share}>
      <img className="share-icon" src={require('Imgs/share-icon.png')} alt=""/>
      <span className="share-title">{sharetitle}</span>
    </div>
  )
}