import React from 'react';
import '../../../../public/config';
import './index.scss';
import { getUrlParam } from 'Utils';

export default props => {
  const { content, history, location, isClick } = props;

  const prodetail = () => {
    if(isClick){
      if(location.search){
        history.push(`/prodetail${location.search}&proId=` + content.productId)
      }else{
        history.push(`/prodetail?proId=`+ content.productId)
      }
    }else{
      return
    }
    
  }
  return (
      <div className="publicoffer" onClick={prodetail}>
        {
          location.pathname === '/publicOffer' || '/canlendar'?
            <div className="top-title">
              {
                content.isNew === 1 ?
                <span className="new-tag">新发</span>
                : <span className="tag-border"></span>
              }
              
              <div className="title">
                {content.productName}
              </div>
            </div>
          :<div className="top-content">
              <div className="abstract">
                <div className='sub-title'>产品概要: </div>
                {content.productId}
              </div>
              <div className="company">
                <div className='sub-title'>公司: </div>
                {content.company}
              </div>
            </div>
        }
        <div className="pub-content">
          <div className="productmainpoint">
            <div className="productmainpointword">产品要点</div>
            {
              content.productReason?content.productReason.split(',').map((item,i) => (
                <div className="sum-item" key={i}>{item}</div>
              ))
              :null
            }
          </div>
          <div className="marketkeypoint">
              <div className="marketkeypointword">营销要点</div>
              <div className="specificmarketkeypoint">
                {
                  content.point ?content.point.split(',').map((item,i) => (
                    <div className="point-item" key={i}>{item}</div>
                  ))
                  :null
                }
              </div>
          </div>
        </div>
      </div>
  );
};