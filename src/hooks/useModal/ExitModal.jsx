import React, { useState, useCallback, useEffect } from 'react';

import { Button } from 'Components/ui';
import useInterval from 'Hooks/useInterval';

export default props => {
    const [duration, setDuration] = useState(null);
    const [leftTime, setLeftTime] = useState(10);

    const { onClose } = props;

    const clear = useCallback(() => {
        setDuration(null);
        setLeftTime(10);
    }, []);

    useEffect(() => {
        setDuration(1000);
        return clear;
    }, [clear]);

    const intervalCallback = useCallback(() => {
        setLeftTime(prev => {
            if (prev === 1) {
                setDuration(null);
                onClose();
                window.sendData({
                    actionType: 'log',
                    sync: true,
                    extra: {
                        EventType: 'FE_Close_Connect'
                    }
                });
                window.closeConnect('很抱歉，由于您长时间没有操作，保险培训师已经退出');
            }
            return prev <= 0 ? prev : prev - 1;
        });
    }, [onClose]);

    useInterval(intervalCallback, duration);

    const continueTraining = () => {
        onClose();
        window.sendData({
            actionType: 'log',
            sync: true,
            extra: {
                EventType: 'Cancel_Close_Connect'
            }
        });
    };

    return (
        <div className="da-modal">
            <div className="content">
                <div>
                    由于您长时间没有操作，保险培训师将在 <span>{leftTime}</span> 秒后自动退出
                </div>
            </div>
            <div className="exit-footer">
                <Button onClick={continueTraining}>
                    继续培训
                </Button>
            </div>
        </div>
    );
};
