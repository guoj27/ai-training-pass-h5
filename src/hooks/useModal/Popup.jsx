import React from 'react';

import { Button } from 'Components/ui';

export default props => {
    const { data, onClose } = props;

    const handleAction = text => {
        window.sendData({ text });
        onClose();
    };

    const { title, body, confirmText, cancelText } = data;
    return (
        <div className="da-modal">
            {title && (
                <div className="title">{title}</div>
            )}
            <div className="content">{body}</div>
            <div className="footer">
                {cancelText && (
                    <Button type="outline" onClick={() => handleAction(cancelText)}>
                        {cancelText}
                    </Button>
                )}
                <Button onClick={() => handleAction(confirmText)}>
                    {confirmText}
                </Button>
            </div>
        </div>
    );
};
