/*
* 使用 AudioContext 处理音频
*/
import { useEffect, useRef, useCallback } from 'react';
import { Toast } from 'antd-mobile';

import { sleep, getUUID } from 'Utils';
import Recorder from './Recorder';

let speakStartTime = 0;
let recordRequestId = null;
let audioSequence = 0;

export default function (undefinded, onCreateMediaSuccess, onCreateMediaFail) {
    const recorder = useRef(null);
    const mediaStream = useRef(null);

    const sendAudio = useCallback((wavBlob, sequence) => {
        function transformArrayBufferToBase64(buffer) {
            let binary = '';
            const bytes = new Uint8Array(buffer);
            for (let len = bytes.byteLength, i = 0; i < len; i++) {
                binary += String.fromCharCode(bytes[i]);
            }
            return window.btoa(binary);
        }

        const reader = new FileReader();
        reader.addEventListener('load', async function (evt) {
            const bWav = transformArrayBufferToBase64(evt.target.result);
            // if (bWav.length > 58) {
            //     const findEmptyStr = bWav.slice(58).split('').filter(el => el !== 'A').filter(el => el !== '=');
            //     const isEmptyVoice = findEmptyStr.length === 0;
            //     console.log('是否是空音频', isEmptyVoice);
            //     console.log('音频前10位', findEmptyStr.slice(0, 10).join(''));
            // }
            window.sendPrimaryData({
                requestId: recordRequestId,
                sequence,
                type: 'audio',
                format: 'wav',
                audio: bWav
            }, false);
        });
        reader.readAsArrayBuffer(wavBlob);
    }, []);

    const datavaliable = useCallback(blob => {
        audioSequence++;
        sendAudio(blob, audioSequence);
    }, [sendAudio]);

    // getUserMedia 版本兼容
    const initUserMedia = useCallback(() => {
        if (navigator.mediaDevices === undefined) {
            navigator.mediaDevices = {};
        }
        if (navigator.mediaDevices.getUserMedia === undefined) {
            navigator.mediaDevices.getUserMedia = constraints => {
                const getUserMedia = navigator.getUserMedia
                    || navigator.webkitGetUserMedia
                    || navigator.mozGetUserMedia;

                if (!getUserMedia) {
                    return Promise.reject(new Error('浏览器不支持 getUserMedia !'));
                }
                return new Promise(function (resolve, reject) {
                    getUserMedia.call(navigator, constraints, resolve, reject);
                });
            };
        }
    }, []);

    const createRecorder = useCallback(() => {
        initUserMedia();
        navigator.mediaDevices.getUserMedia({
            audio: {
                sampleRate: 16000,        // 采样率
                channelCount: 1,          // 声道
                audioBitsPerSecond: 16,   // 输出采样数位 8, 16;
                volume: 1.0,              // 音量
                autoGainControl: true
            }
        })
            .then(stream => {
                mediaStream.current = stream;
                onCreateMediaSuccess();
            })
            .catch(err => {
                // Toast.fail('开启录音失败，请刷新重试');
                onCreateMediaFail();
                console.error(err);
            });
    }, [initUserMedia, onCreateMediaSuccess, onCreateMediaFail]);

    const startRecord = useCallback(() => {
        recorder.current && recorder.current.closeAudioContext();
        recorder.current = new Recorder(mediaStream.current, {}, datavaliable);
        recorder.current.start();

        recordRequestId = getUUID();
        window.dhen.recordRequestIds.push(recordRequestId);
        audioSequence = 0;
        speakStartTime = new Date().getTime();
    }, [datavaliable]);

    const stopRecord = useCallback(async () => {
        const speakTime = new Date().getTime() - speakStartTime;

        if (speakTime < 200) {
            Toast.fail('请试着录音时间长一些');
            recorder.current.stop();
            return;
        }

        await sleep(300);     // 多录1s, 否则吃字
        recorder.current.stop();

        await sleep(200);
        window.sendPrimaryData({
            requestId: recordRequestId,
            type: 'audio_interrupt'
        }, false);

        // download();
    }, []);

    const download = useCallback(() => {
        if (recorder.current === null) {
            Toast.fail('请先录音');
            return;
        }
        const src = recorder.current.wavSrc();
        const a = document.createElement('a');
        a.href = src;
        a.download = 'ar';
        a.click();
    }, []);

    useEffect(() => {
        createRecorder();
        return () => {
            recorder.current && recorder.current.closeAudioContext();
            if (mediaStream.current) {
                mediaStream.current.getTracks().forEach(track => {
                    track.stop();
                });
            }
        };
    }, [createRecorder]);

    return {
        startRecord,
        stopRecord
    };
}
