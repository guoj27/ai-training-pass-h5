import React from 'react';
import { BrowserRouter, Route, Switch, HashRouter } from 'react-router-dom';

import containers from './routes';
import { publicPath } from 'EnvConfig';


const AppRouter = () => (
    <HashRouter basename={publicPath}>
        <Switch>
            <Route path={'/'} exact component={containers.Redirect} />
            <Route path={'/login'} component={containers.Login} />
            <Route path={'/card'} component={containers.Mock} />
            <Route path={'/open'} component={containers.Open} />
            <Route path={'/pyramid'} component={containers.Pyramid} />
            <Route path={'/publicOffer'} component={containers.PublicOffer} />
            <Route path={'/prodetail'} component={containers.Prodetail} />
            <Route path={'/canlendar'} component={containers.Canlendar} />
            <Route path={'/intro'} component={containers.Introducation} />
        </Switch>
    </HashRouter>
);

export default AppRouter;
