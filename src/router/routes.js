import React from 'react';
import loadable from '@loadable/component';
import { PageLoading } from 'Components/ui';

const loadFallback = { fallback: <PageLoading /> };
const containers = {
    Redirect: loadable(() => import(/* webpackChunkName: 'Redirect' */'../containers/Redirect'), loadFallback),
    Login: loadable(() => import(/* webpackChunkName: 'Mock' */'../widgets/Login'), loadFallback),
    Open: loadable(() => import(/* webpackChunkName: 'Mock' */'../widgets/Openindex'), loadFallback),
    Mock: loadable(() => import(/* webpackChunkName: 'Mock' */'../containers/Mock'), loadFallback),
    Pyramid:loadable(() => import(/* webpackChunkName: 'Test2' */'../widgets/WeeklyProductTraining'), loadFallback),
    PublicOffer:loadable(() => import(/* webpackChunkName: 'Test2' */'../widgets/PublicOfferingFundHomepage'), loadFallback),
    Prodetail:loadable(() => import(/* webpackChunkName: 'Test2' */'../widgets/ProDetail'), loadFallback),
    Introducation:loadable(() => import(/* webpackChunkName: 'Test2' */'../widgets/Introducation'), loadFallback),
    Canlendar:loadable(() => import(/* webpackChunkName: 'Test2' */'../widgets/CanlendarIndex'), loadFallback),
    NotFound: loadable(() => import(/* webpackChunkName: 'NotFound' */'../containers/NotFound'), loadFallback),

};

export default containers;
