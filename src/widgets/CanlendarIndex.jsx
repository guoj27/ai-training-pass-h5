import React, { useState, useEffect } from 'react';
import api from '../axios/api';
import DisplayCard from '../components/ui/DisplayCard/index';
import SlideCard from '../components/ui/SlideCard/index';
import { getUrlParam, getWeekDay } from 'Utils';
import Aicard from '../components/ui/Aicard/index'

import './canlendarIndex.scss';

export default props => {
  const dateArr = [{'Mo':'一'}, {'Tu':'二'}, {'We':'三'},{'Th': '四'}, {'Fr':'五'}, {'Sa':'六'},{'Su':'日'},{'all':'全部'}];
  const [productInfo, setProductInfo] = useState([]);
  const [pyramidConfigs, setPyramidConfigs] = useState([]);
  const [curWeek, setCurWeek] = useState(null);
  const [curProtype, setCurProtype] = useState(null);
  
  useEffect(() => {
    getPyramidConfigs();
    setCurProtype(parseInt(getUrlParam().configId));
    setCurWeek(parseInt(getUrlParam().curDate));
    // getProductInfoByScaleTime(getWeekDay(getUrlParam().curDate), getUrlParam().proType);
    getProductInfoByScaleTime(parseInt(getUrlParam().curDate), parseInt(getUrlParam().configId));
  },[])

	//每天数据
	const getProductInfoByScaleTime = (date,type) => {
    let params;
		if(date != dateArr.length-1){
			params = {
				'mainType':type,
        'ipoStartDate':getWeekDay(date)
			}
		}else{
			params = {
				'mainType':type,
				// 'all':'all'
			}
		}
		api.getProductByType(params).then(res => {
			setProductInfo(res);
		});
  }


  const getPyramidConfigs = () => {
    api.getPyramidConfigs().then(res => {
			setPyramidConfigs(res)
		});
  }

  const dateSelect = i => {
    setCurWeek(i)
    getProductInfoByScaleTime(i, curProtype);
  }

  const typeClick = i => {
    setCurProtype(i);
    getProductInfoByScaleTime(curWeek, i);
  }
  return(
    <div className="canlendar">
      <Aicard></Aicard>
      <DisplayCard title={'本周全部产品'} displaythumb={false} displaybutton={true} {...props} >
        <div className="whatdaychoicelower">
          {
            dateArr.map((item,i) => (
            <div className={`word ${i} ${i === curWeek ? 'active-word': ''}`} key={i} onClick={() => dateSelect(i)}>
              <div>{Object.values(item)}</div>
              {
                getWeekDay(i).slice(-2,-1)=='0'?
                <div>{getWeekDay(i).slice(-1)}</div>
                :
                <div>{getWeekDay(i).slice(-2)}</div>
              }
            </div>
            ))
          }
        </div>
        <div className="pro-type">
          {
            pyramidConfigs.map((item, i) => (
              <div className={`item ${item.configId === curProtype ? 'active-item': ''}`} key={i} onClick={() => typeClick(item.configId)}>{item.productType}</div>
            ))
          }
        </div>
          {
            productInfo.length!=0 ?
            productInfo.map((item,i) => (
              <SlideCard content={item} key={i} {...props} isClick={true} ></SlideCard>
            ))
            : <div className="date-tip">暂无数据</div>
          }
        
      </DisplayCard>
    </div>
  )
}