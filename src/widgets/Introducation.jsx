import React, {useRef, useState, useEffect} from 'react';
import api from '../axios/api';
import Aicard from '../components/ui/Aicard/index';
import moment from 'moment';

import './index.scss';

export default props => {
  const { history, location } = props;

  const btnTitle = ['基金投资策略','市场综合观点'];
  const [audio1, setAudio1] = useState();
  const [audio2, setAudio2] = useState();
  const videoTime = useRef(32);
  const [isbtn1Show, setIsbtn1Show] = useState("none");
  const [isbtn2Show, setIsbtn2Show] = useState("none");
  const [isPlayVoice, setPlayVoice] = useState(false);
  const [thisSource, setThisSource] = useState('');
  const [productInfo, setProductInfo] = useState([]);
  const [isVideoPause, setIsVideoPause] = useState(true);
  const [strategy, setStrategy] = useState();
  const [currentData, setCurrentData] = useState();
  
  const audio1Ref = useRef(null);
  const audio2Ref = useRef(null);

  useEffect(() => {
    setCurrentData(moment(new Date()).format("YYYYMMDD"));

    api.getStrategy().then(res => {
      setStrategy(res);
    });

  },[])

  useEffect(() => {
    audio1Ref.current && setAudio1(audio1Ref.current) ;
  },[audio1Ref])

  useEffect(() => {
    audio2Ref.current && setAudio2(audio2Ref.current) ;
  },[audio2Ref])

  const showBtn = key => {
    if(key === 0){
      setIsbtn1Show("block")
    }else{
      setIsbtn2Show("block")
    }
  }
  
  // const goChat = i => {
  //   history.push(`/prodetail?empId=` + sessionStorage.getItem("employeeId")+'&proId='+id)
  // }

  const play1 = () => {
    pause2();
    audio1 && audio1.play()
    .then(() => {
        console.log('播放ok');
    })
    .catch(err => {
        console.error('音频播放错误：', err);
    });
  };

  const pause1 = () => {
    audio1 && audio1.pause();
  };

  const play2 = () => {
    audio2 && audio2.play()
    .then(() => {
        console.log('播放ok');
    })
    .catch(err => {
        console.error('音频播放错误：', err);
    });
  };

  const pause2 = () => {
    audio2 && audio2.pause();
  };

  const onAudio1Play = () => {
    audio1 && audio1.play();
    pause2();                                                                                                                                     
  }

  const onAudio1Ended = () => {
    setThisSource('');
    setPlayVoice(false);
  }

  const onAudio2Play = () => {
    audio2 && audio2.play();
    pause1();                                                                                                                                     
  }

  const onAudio2Ended = () => {
    setThisSource('');
    setPlayVoice(false);
  };

  const proDetail = () => {
    history.push('/pyramid'+location.search );
  }

  return (
    <div className="ai-card">
      {strategy && <Aicard></Aicard>}
      
      <div className="assets-interprete">
          <div className="assets-banner">
            <div className="banner-text"> 为您播报 · 本周市场摘要</div>
            <img className="banner-icon" src={require("Imgs/ex.png")}></img>
          </div>
          <div className="assets-box">
            <div className="init-audio">
              <audio
                className="audiocsschange"
                id="audio-1"
                controls
                src={strategy && `https://spdb.cdn.bcebos.com/WeeklyMarketInfo/${strategy.week}/MarketInfo/marketinfo_1.wav`}
                onPlay={onAudio1Play}
                onEnded={onAudio1Ended}
                // style={{backgroundColor:"yellow"}}
              />
            </div>

            <div className="btn-show recommend-item" style={{display: isbtn1Show}}>
              <div className="show-title">
                <img src={require(`Imgs/btn1.png`)} alt=""/>
                {btnTitle[0]}
              </div>
              <div className="show-content" >
                <img src={strategy && require(`Imgs/weeklyView/${strategy.marketView}.png`)} alt=""/>
              </div>
            </div>

            <div className="btn-show recommend-item" style={{display: isbtn2Show}}>
              <div className="show-title">
                <img src={require(`Imgs/btn2.png`)} alt=""/>
                {btnTitle[1]}
              </div>
              <div className="show-content" >
                {strategy && strategy.strategyView}
              </div>

            </div>
            <ul className="assets-text">
              <div className="action-btn">
                {
                  btnTitle.map((item,i) => (
                    <div className='btn-cs' onClick={() => showBtn(i)} key={i}>
                      <img src={require( `Imgs/btn${i+1}.png`)} alt=""/>
                      {item}
                    </div>
                  ))
                }
              </div>
            </ul>
          </div>

          {/* <div className="recommend">
            <img className="recommend-icon" src={require('Imgs/intro-icon.png')} alt=""/>
            <ul className="recommend-content">
              {
                productInfo && productInfo.map((item,i) => (
                  <li className="recommend-item" key={i} onClick={() => goChat(i)}>
                    <div className="top-content">
                      <span className="new-tag">新发</span>
                      <span className="fund-name">{item.productName}</span>
                    </div>
                    <div className="bottom-cotent">
                      <div className="rate-box">
                        <div>{item.product_id}</div>
                        <div>近一年收益率</div>
                      </div>
                      <div className="fund-tag">
                        {
                          item.point.split(',').map((item,i) => (
                            <span className="border-box" key={i}>{item}</span>
                          ))
                        }
                        <div className="border-box purchase">最低申购: ${item.purchase}</div>
                      </div>
                    </div>
                  </li>
                ))
              }
            </ul>
          </div> */}
          <div className="btn-pro" onClick={() => proDetail()}>
            <div >每周重点产品</div>
            <div className="btn-xiaopu">
              <img src={require('Imgs/btn-xiaopu.png')} alt=""/>
            </div>
          </div>
        </div>
    </div>
  );
};
