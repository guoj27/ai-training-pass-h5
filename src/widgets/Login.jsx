
import React, {useRef, useState, useCallback, useEffect} from 'react';
import api from '../axios/api';
import { Button, InputItem, Toast } from 'antd-mobile';
import { isWeChat, publicPath, deployEnv} from 'EnvConfig';
import {  getUrlParam, encrypt } from 'Utils';

import './Login.scss';

const cacheImgs = [
  require('Imgs/video-poster.png'),
  require('Imgs/video-poster.gif'),
  require('Imgs/ex.png'),
  require('Imgs/banner-card.png'),
  require('Imgs/head.png'),
  require('Imgs/login-text.png'),
  require('Imgs/play.png'),
  require('Imgs/audio.png'),
  require('Imgs/audio.gif'),
  require('Imgs/pyramid.png'),
  require('Imgs/chat-left-bg.png'),
  require('Imgs/input-content.png'),
  require('Imgs/login-banner.png'),
  require('Imgs/login-bg.mp4'),
  require('Imgs/login-bg.png'),
  require('Imgs/mainpoints.png'),
  require('Imgs/summary-bg.png'),
  
  
];

export default props => {

  const [errorType, setErrorType] = useState(null);
  const [telValue, setTelValue] = useState(localStorage.getItem("tel"));
  const [idValue, setIdValue] = useState(localStorage.getItem("employeedId"));
  const [checkCode, setCheckCode] = useState("");
  const [video, setVideo] = useState();

  const videoRef = useRef(null);

  const { history } = props;

  // const idleVideos = window.$gVar.idleVideos || [];
  const [loginButton, setLoginButton] = useState(false);
  const [duration, setDuration] = useState(30);

  const base64ImgPrefix = "data:image/png;base64,";
  const [imageUrl, setImageUrl] = useState("");
  const [sessionId, setSessionId] = useState("");

  const isDev = deployEnv === 'dev';

  // 注入微信权限验证配置
  const configWX = useCallback(async onError => {
    // if(getUrlParam().oldId){
    //   sessionStorage.setItem('refId', getUrlParam().oldId);
    // }else{
    //   sessionStorage.setItem('refId', "");
    // }

    const fetchParams = await api.getPufaWXConfig()       // getPufaWXConfig | getAnluojieWXConfig
      .then(data => data)
      .catch(err => {
          onError(err || '获取微信签名失败');
      });

    if (!fetchParams) {
        return;
    }

    window.configSuccess = true;

    const configParams = {
        appId: fetchParams.appId,
        jsApiList: ['updateAppMessageShareData','onMenuShareAppMessage','getNetworkType'],
        nonceStr: fetchParams.nonceStr,
        signature: fetchParams.signature,
        timestamp: fetchParams.timestamp,
        debug: false
    };

    window.wx.config(configParams);

    window.wx.error(res => {
      console.log('微信配置信息验证失败：', res);
      configSuccess = false;
      // 生产上打开
      _this.wsError('微信配置信息验证失败, 请使用浏览器打开网页');
    });

    window.wx.ready(() => {
      console.log('wx config ready');
      // 生产上注释
      // _this.setState({
      //   isCloseWs: false
      // });

      console.log('configSuccess: ', configSuccess);
      if (configSuccess) {

        const employeeId = sessionStorage.getItem('employeeId');
        let linkUrl = window.origin + publicPath + '#login?refId' + employeeId;

        // 需在用户可能点击分享按钮前就先调用
        window.wx.onMenuShareAppMessage({
          title: 'Ai通关',
          desc: 'Ai通关',
          // link: linkUrl,
          imgUrl: window.location.origin + publicPath + 'redirect-img.jpeg',
          success: function () {
            console.log('微信分享设置成功');
          }
        });
      }
    });
  }, []);

  useEffect(() => {
    if(isWeChat ){
      configWX();
      // document.addEventListener('WeixinJSBridgeReady',function(){
      //   document.getElementById('shakeVideo').play();
      // },false);
    }

    for (let i = 0; i < cacheImgs.length; i++) {
      const cacheImg = cacheImgs[i];
      const imgELe = document.createElement('img');
      imgELe.src = cacheImg;
    }
    getImgcode();
    return () => {
      setDuration(null);
    }
  },[])

  useEffect(() => {
    videoRef.current && setVideo(videoRef.current);
    videoRef.current && videoRef.current.play();
  },[videoRef]);

  const telChange = value => {
    setTelValue(value)
  };

  const idChange = value => {
    setIdValue(value)
  };

  const codeChange = value => {
    setCheckCode(value);
  }

  const login = () => {
    if (checkCode.length==4) {
      // 去除中间和头尾的空格
      const id = idValue.replace(/\s*/g, '').replace(/^\s*|\s*$/g, '');
      const tel = telValue.replace(/\s*/g, '').replace(/^\s*|\s*$/g, '');

      if (!id) {
        Toast.fail('工号不能为空', 1);
        return;
      }

      if (!tel) {
        Toast.fail('手机号不能为空', 1);
        return;
      }

      if (!isDev && !checkCode) {
        Toast.fail('验证码不能为空', 1);
        return;
      }

      setLoginButton(true);

      const values = {
        sessionId,
        checkCode,
        employeeId: encrypt(id),
        mobile: encrypt(tel),
        refId: getUrlParam().refId,
        aid: ''
      }
      const fetchFn = isDev ? api.login : api.login;
      fetchFn(values).then(resData => {
        if (resData) {
          sessionStorage.setItem("token", resData);
          const refId = getUrlParam().refId;
          if (refId) {
            sessionStorage.setItem("refId", refId);
          }
          sessionStorage.setItem("employeeId", id);
          sessionStorage.setItem("tel", tel);
          sessionStorage.setItem('logined', true);
          history.push('/open');
        } else {
          getImgcode();
          setLoginButton(false);
        }
      }).catch(err => {
        getImgcode();
        console.log(err,'err');
        setErrorType(err)
      })
      
    }
  }

  const getImgcode = () => {

    setCheckCode("");
    api.getImgcode().then(resData => {
      setImageUrl(resData.imageBase64);
      setSessionId(resData.sessionId);
    })
  }

  const onVideoPlay = () => {
    console.log("video play")
    video && video.play();
  }

  return (
    <div className="login-bg">
      <div className="video-bg">
        <video
          id="shakeVideo"
          ref={videoRef}
          onPlay={onVideoPlay}
          autoPlay
          loop
          muted="muted"
          playsInline
        >
          <source  src={require('Imgs/login-bg.mp4')}></source>
        </video>
      </div>
      <div className="login-banner">
        <img src={require("Imgs/login-banner.png")} alt=""/>
      </div>
      <div className="input-content">
        <div className="login-text">
          {/* <img src={require("Imgs/login-text.png")}></img> */}
          每周市场与产品
        </div>
        <div className="form-item emp">
          <img src={require("Imgs/emp.png")}></img>
          <InputItem
          placeholder="工号"
          value = {idValue}
          className="login-input num-input"
          onChange={idChange} />
        </div>
        <div className="form-item">
          <img src={require("Imgs/tel.png")}></img>
          <InputItem
            placeholder="手机号"
            value = {telValue}
            onChange={telChange}
            className="login-input tel-input"
            maxLength='11'
            />
        </div>
        <div className="invaild">
          <div className="form-item">
            <InputItem
              className="login-input code-input"
              placeholder="验证码"
              value = {checkCode}
              onChange = {codeChange}
            />
          </div>
          <span
              data-stand-id="captcha_refresh"
              className="captcha-img"
              onClick={getImgcode}
            >
              {imageUrl && <img alt="加载失败" src={`${base64ImgPrefix}${imageUrl}`} />}
            </span>
        </div>
        <div className="errorTips">{errorType}</div>
        <div className="bottom-action">
          <Button onClick={login} className="login-btn">
            登 录
          </Button>
          <img className="login-tio" src={require("Imgs/login-tip.png")}></img>
        </div>
      </div>
    </div>

  )
}

