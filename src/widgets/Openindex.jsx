import React, { useState, useEffect, useRef, useCallback} from 'react';
import api from '../axios/api';
import ShareButton from '../components/ui/ShareButton/index';
import useModal from 'Hooks/useModal';
import { isWeChat, publicPath } from 'EnvConfig';
import BottomStatisticsBar from '../components/ui/BottomStatisticsBar/index';
import 'antd/dist/antd.css';
import { Toast } from 'antd-mobile';
import moment from "moment";
import { getUrlParam } from 'Utils';

import './Openindex.scss'

export default props => {
  const { history } = props;
  const [isbtnShow, setIsbtnShow] = useState("none");
  const [showBtnKey,setShowBtnKey] = useState(0);
  const [isPlayVoice, setPlayVoice] = useState(false);
  const [isAutoPlay, setIsAutoPlay] = useState(false);
  const [thisSource, setThisSource] = useState('');
  const [curCAudio, setCurAudio] = useState();
  const [audio, setAudio] = useState();
  const [source, setSource] = useState('');
  const [markets,setMarkets] = useState([]);
  const [pcmInfo,setPcmInfo] = useState({});
  const [strategy, setStrategy] = useState();

  const btnList = ['基金投资策略','市场综合观点'];

  const { onError } = useModal();

  const audioRef = useRef(null);


  useEffect(() => {
      if (!sessionStorage.getItem('logined')) {
          onError('请先登录');
          props.history.push('/');
      }
      // sessionStorage.removeItem('logined');

      let currentDate = moment(new Date()).format("YYYY-MM-DD");
      let employeeId = sessionStorage.getItem("employeeId") || getUrlParam().empId;

      Toast.loading('Loading...', 30,);

      api.getMarketInfo({'currentDate':currentDate})
      .then(res => {
        setMarkets(res.returnMsg);
        Toast.hide();
      });
      api.getPcmInfo(employeeId).then(res => setPcmInfo(res));

      api.getStrategy().then(res => {
        setStrategy(res)
      });
      
  }, []);

  useEffect(() => {
    

    audioRef.current && setAudio(audioRef.current) ;
  },[audioRef]);

  const onPlayAudioClick = (i) => {
    setSource(markets[i].infoIndex);
    const audio = document.getElementById("audio");
    if(i === curCAudio){
      if(audio.paused){
        setPlayVoice(true);
        setIsAutoPlay(true);
        setCurAudio(i);
        setThisSource(markets[i].infoIndex);
        play();

      }else{
        pause();
        setPlayVoice(false);
        setIsAutoPlay(false);
        setThisSource();
        setSource();
      }
    }else{
      setPlayVoice(true);
      setIsAutoPlay(true);
      setCurAudio(i);
      setThisSource(markets[i].infoIndex);
      setSource(markets[i].infoIndex);
      play();
    }

  }

  const onAudioPlay = () => {
    audio && audio.play()
  }

  const onAudioEnded = () => {
    setThisSource('');
    setPlayVoice(false);
  }

  const play = () => {
    setPlayVoice(true);
    audio && audio.play()
    .then(() => {
        console.log('播放ok');
    })
    .catch(err => {
        console.error('音频播放错误：', err);
    });
  };

  const pause = () => {
    audio && audio.pause();
    setPlayVoice(false);
    // setThisSource('');
    // onAudioEnded();
  };

  const networkhealth=()=>{
    window.wx.getNetworkType({
      success: function (res) {
        alert(res.networkType);
        console.log('lalalalalalalal')
      },
      fail: function (res) {
        alert(JSON.stringify(res));
        console.log('lulululululululu')
      }
    });
  }



  const showBtn = (key,title) => {
    setIsbtnShow("block");
    setShowBtnKey(key);

    let scrollTarget = document.getElementById("app");
    scrollTarget.scrollTop = 0;
  }

  const proDetail = () => {
    history.push('/pyramid');
  }

  const shareClick = () => {
    history.push('/intro?empId='+sessionStorage.getItem("employeeId"))
  }

  return (
    <div className="open-index">
      <audio
        id="audio"
        autoPlay={isAutoPlay}
        ref={audioRef}
        preload="auto"
        src={thisSource ? `https://spdb.cdn.bcebos.com/WeeklyMarketInfo/${markets[0].week}/MarketInfo/marketinfo_${source}.wav` : ""}
        onPlay={onAudioPlay}
        onEnded={onAudioEnded}
      />
      <div className="top-content">
        <div className="pcm-info">
          <div className="pcm-icon">
            <img src={require('Imgs/head.png')} alt=""/>
          </div>
          <div className="pcm-intro">
            <div className="pcm-name">{pcmInfo.employeeName}</div>
            <div className="pcm-num">{pcmInfo.dictName||'理财经理'} {pcmInfo.employeeId}</div>
            <div className="pcm-branch">{pcmInfo.orgName}</div>
          </div>
        </div>
        {/*<div className="pcn-info">*/}
        {/*  <div className="pcn-rate">*/}
        {/*    <Progress*/}
        {/*      type="circle"*/}
        {/*      percent={62.53}*/}
        {/*      strokeWidth={10}*/}
        {/*      width= '48px'*/}
        {/*      size = 'small'*/}
        {/*      trailColor='#EEF1FA'*/}
        {/*      strokeColor={{*/}
        {/*        '0%' :'#EB1268',*/}
        {/*        '100%':'#FF2E6B'*/}
        {/*      }}*/}
        {/*    />*/}
        {/*  </div>*/}
        {/*  <div className="rate-text">*/}
        {/*    <div className="rate-title">销售完成率</div>*/}
        {/*    <div className="rate-content">打败全行65.32%的员工</div>*/}
        {/*  </div>*/}
        {/*</div>*/}
      </div>

      <div className="view-content">
        <div className="view-top">
          <ShareButton sharetitle="发给客户" {...props} shareClick={shareClick}/>
          <div className="view-title">每周市场一览</div>
        </div>

        <div className="assets-box">
          <div className="btn-show recommend-item" style={{display: isbtnShow}}>
            <div className="show-title">
              <img src={require(`Imgs/btn${showBtnKey + 1}.png`)} alt=""/>
              {btnList[showBtnKey]}
            </div>
            <div className="show-content">
              {
                showBtnKey === 1 ?
                <img src={require(`Imgs/weeklyView/${strategy.marketView}.png`)} alt=""/>
                :  strategy && strategy.strategyView
              }
            </div>
          </div>

          <ul className="assets-text">
            {
              markets.map((item,i) => (
                <li className="assets-item" key={i}>
                  <div className="left-asset">
                    <div className="asset-title">{item.title}</div>
                    <span className="asset-keyword" key={i}>{item.content}</span>
                  </div>
                  <div className="asset-audio" onClick={() => onPlayAudioClick(i)}>
                    <img src={require(isPlayVoice && curCAudio ===i ? `Imgs/audio.gif` : 'Imgs/audio.png')} alt=""/>
                    {/* <span className={"audio-duration"+i}>{`${Math.floor(item.duration/60)}'${item.duration%60}''`}</span> */}
                    <span className={"audio-duration"}>{isPlayVoice && curCAudio ===i ? '暂停': '播放'}</span>
                  </div>
                  {/* <div className="asset-audio" onClick={() => onPlayAudioClick(i)}>
                    <div className={`wifi-symbol ${isPlayVoice && curCAudio ===i && thisSource? 'active-audio' : ''}`} >
                        <div className="wifi-circle first"></div>
                        <div className="wifi-circle second" ></div>
                        <div className="wifi-circle third" ></div>
                    </div>
                    <span className={"audio-duration"+i}>{item.duration}</span>
                  </div> */}
                </li>
              ))
            }
            <div className="action-btn">
              {
                btnList.map((item,i) => (
                  <div className='btn-cs' key={i} onClick={() => showBtn(i,item)}>
                    <img src={require(`Imgs/btn${i+1}.png`)} alt=""/>
                    {item}
                  </div>
                ))
              }
            </div>
          </ul>
        </div>
        <div className="btn-pro" onClick={() => proDetail()}>
            <div >每周重点产品</div>
            <div className="btn-xiaopu">
              <img src={require('Imgs/btn-xiaopu.png')} alt=""/>
            </div>
          </div>
      </div>

      

      <BottomStatisticsBar></BottomStatisticsBar>
    </div>
  )
}