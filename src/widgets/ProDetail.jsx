import React, {useRef, useState, useEffect} from 'react';
import api from '../axios/api';
import { deployEnv} from 'EnvConfig';
import DisplayCard from '../components/ui/DisplayCard/index';
import SlideCard from '../components/ui/SlideCard/index';
import Aicard from '../components/ui/Aicard/index';
import { getUrlParam } from 'Utils';
import { Toast } from 'antd-mobile';
import Bottomstatisticsbar from '../components/ui/BottomStatisticsBar/index';
import './ProDetail.scss';
import qs from 'qs';


export default props => {
  const { history, location }=props;
  const chatContent = useRef([]);
  const audioRef = useRef(null);

  const [chatBubble, setChatBubble] = useState([]);
  const [isPlayVoice, setPlayVoice] = useState(false);
  const [curCAudio, setCurAudio] = useState();
  const [isAutoPlay, setIsAutoPlay] = useState(false);
  const [detaildata, setDetaildata]  = useState();
  const [tel,setTel] = useState(null);
  const [partnerBackUrl, setPartnerBackUrl] = useState(null);
  const [audio, setAudio] = useState();
  const [duration, setDuration] = useState();
  const [thisSource, setThisSource] = useState('');
  const [source, setSource] = useState();

  const [tablist, setTablist] = useState([])

  const isDev = deployEnv === 'dev';
  const param = {
    mainType: getUrlParam().configId,
    productId : getUrlParam().proId
  }

  useEffect(() => {
    let weaEmpid=getUrlParam().empId;
    console.log('weaEmpid',weaEmpid)
    if (weaEmpid=='undefined') {
        if (!sessionStorage.getItem('logined')) {
            // onError('请先登录');
            props.history.push('/');
        }
    }
    Toast.loading('Loading...', 30,);
		api.getProductById(param).then(res => {
      console.log('1111111111111111',res)
      setDetaildata(res)
      Toast.hide();
    });
    
    api.getPcmInfo(sessionStorage.getItem("employeeId") || getUrlParam().empId).then(res => setTel(res.mobile));

    getBuylink();
    
	},[])

  const send = (i,item) => {
    pause();
    api.getProductById(param).then(res => {
      setTablist(eval(res.tabList))
      const tab = {"tab": item};
      Object.assign(res,tab)
      chatContent.current = [...chatContent.current, {'right':item},{'left':res}];
      setChatBubble([...chatBubble, {'right':item},{'left':res}]);
    });
    
  }

  useEffect(()=>{
    let scrollTarget = document.getElementsByClassName("pro-detail")[0];
    scrollTarget.scrollTop = scrollTarget.scrollHeight;
  },[chatContent.current.length]);

  useEffect(() => {
    audioRef.current && setAudio(audioRef.current) ;
    const audioDom = document.getElementById("audio");
    setDuration(audioDom.duration);

  },[audioRef]);

  const onPlayAudioClick = (i,item) => {
    for(let i=0; i<tablist.length; i++){
      if(Object.keys (tablist[i])[0] === item) {
        setSource(detaildata.week + '/ProductInfo'+'/'+getUrlParam().proId+'_'+Object.values (tablist[i]))
      }
    }
    
    const audioDom = document.getElementById("audio");
    if(i === curCAudio){
      if(audioDom.paused){
        setPlayVoice(true);
        setIsAutoPlay(true);
        setCurAudio(i);
        setThisSource(item);
        play();

      }else{
        pause();
        setPlayVoice(false);
        setIsAutoPlay(false);
        setThisSource();
        setSource();
      }
    }else{
      setPlayVoice(true);
      setIsAutoPlay(true);
      setCurAudio(i);
      setThisSource(item);
      for(let i=0; i<tablist.length; i++){
        if(Object.keys (tablist[i])[0] === item) {
          setSource(detaildata.week +'/ProductInfo' +'/'+getUrlParam().proId+'_'+Object.values (tablist[i]))
        }
      }
      play();
    }

  }

  const onAudioPlay = () => {
    audio && audio.play()
  }

  const onAudioEnded = () => {
    setPlayVoice(false);
  }

  const play = () => {
    setPlayVoice(true);
    audio && audio.play()
    .then(() => {
        console.log('播放ok');
    })
    .catch(err => {
        console.error('音频播放错误：', err);
    });
  };

  const pause = () => {
    audio && audio.pause();
    setPlayVoice(false);
    // setThisSource('');
    // onAudioEnded();
  };

  // useEffect(() => {
  //   document.addEventListener('visibilitychange', () => {
  //       const active = document.visibilityState === 'visible';
  //       console.log('应用正处于前台: ', active ? '是' : '否', document.visibilityState);
  //       if (active) {
  //         play();
  //       } else {
  //         pause();
  //       }
  //   });
  // /* eslint-disable */
  // }, []);

  const shareClickDisplay=  () => {
    history.push(location.pathname + location.search + '&empId='+ sessionStorage.getItem("employeeId"));
    getBuylink()
    
  }

  const getBuylink = () => {
    const configId = getUrlParam().configId;
    // const path = isDev ? "etest2" : "wap" ;
    //sit是etest2 uat是etest 生产是wap
    const path = "wap";
    const ManagerId = getUrlParam().empId;
    const FundNo =  getUrlParam().proId || 164701;
    let linkUrl = '';
    // 初代linkurl拼接
    // const backUrl = encodeURIComponent(window.location.origin + window.location.pathname + '#/intro?empId='+ sessionStorage.getItem("employeeId"));
    // if (configId === "1"){
    //   // 基金
    //   linkUrl = `https://${path}.spdb.com.cn/mspmk-web-fund/MarketFundDetail.hl?FundNo=${FundNo}&Productcategory=1&DhManagerID=${ManagerId}&DhBackurl=${backUrl}&H5Channel=400&DigitalSceneID=400007`
    // }else if(configId === "4"){
    //   // 汇理财
    //   linkUrl = `https://${path}.spdb.com.cn/mspmk-cli-structdeposits/ProductDetailh5?Finance_no=${FundNo}&managerId=${ManagerId}&H5Channel=400&from=dhsx&backUrl=${backUrl}`+'&DigitalSceneID=400007'
    // }else if(configId === "5"){
    //   // 大额存单
    //   linkUrl = `https://${path}.spdb.com.cn/mspmk-cli-bigdeposits/Buyh5?H5Channel=400&RecManNumber=${ManagerId}&from=dhsx&productId=${FundNo}&backUrl=${backUrl}`+'&DigitalSceneID=400007'
    // }else if(configId === "6"){
    //   // 安享盈
    //   linkUrl = `https://${path}.spdb.com.cn/mspmk-cli-enjoydeposits/Buyh5?H5Channel=400&RecManNumber=${ManagerId}&EnjoyProductId=${FundNo}&from=dhsx&backUrl=${backUrl}`+'&DigitalSceneID=400007'
    // }else if(configId === "3"){
    //   // 理财
    //   linkUrl = `https://${path}.spdb.com.cn/mspmk-cli-financebuy/productDetails?FinanceNo=${FundNo}&H5Channel=400&FromUrl=${backUrl}&ManagerId=${ManagerId}&TraceNo=08`
    // }
    // 二代linkurl拼接
    const backUrl = window.location.origin + window.location.pathname + '#/intro?empId='+ sessionStorage.getItem("employeeId");
    // 基金
    var objJiJing = {
      FundNo: FundNo,
      Productcategory: "1",
      DhManagerID:ManagerId,
      DhBackurl:backUrl,
      H5Channel:"400",
      DigitalSceneID:"400105",
    }
    var resJiJing = qs.stringify(objJiJing)
    // 汇理财
    var objHuiLiCai = {
      Finance_no: FundNo,
      managerId: ManagerId,
      H5Channel:"400",
      from:"dhsx",
      backUrl:backUrl,
      DigitalSceneID:"400105",
    }
    var resHuiLiCai = qs.stringify(objHuiLiCai)
    // 大额存单
    var objDaECunDan = {
      H5Channel: "400",
      RecManNumber: ManagerId,
      from:"dhsx",
      productId:FundNo,
      backUrl:backUrl,
      DigitalSceneID:"400105",
    }
    var resDaECunDan = qs.stringify(objDaECunDan)
    // 安享盈
    var objAnXiangYing = {
      H5Channel: "400",
      RecManNumber: ManagerId,
      EnjoyProductId:FundNo,
      from:"dhsx",
      backUrl:backUrl,
      DigitalSceneID:"400105",
    }
    var resAnXiangYing = qs.stringify(objAnXiangYing)
    // 理财
    var objLiCai = {
      FinanceNo: FundNo,
      H5Channel: "400",
      FromUrl:backUrl,
      ManagerId:ManagerId,
      // TraceNo:"4105",
      SceneNo:"4105",
    }
    var resLiCai = qs.stringify(objLiCai)
    if (configId === "1"){
      // 基金
      linkUrl = `https://${path}.spdb.com.cn/mspmk-web-fund/MarketFundDetail.hl?`+resJiJing
    }else if(configId === "4"){
      // 汇理财
      linkUrl = `https://${path}.spdb.com.cn/mspmk-cli-structdeposits/ProductDetailh5?`+resHuiLiCai
    }else if(configId === "5"){
      // 大额存单
      linkUrl = `https://${path}.spdb.com.cn/mspmk-cli-bigdeposits/Buyh5?`+resDaECunDan
    }else if(configId === "6"){
      // 安享盈
      linkUrl = `https://${path}.spdb.com.cn/mspmk-cli-enjoydeposits/Buyh5?`+resAnXiangYing
    }else if(configId === "3"){
      // 理财
      linkUrl = `https://${path}.spdb.com.cn/mspmk-cli-financebuy/productDetails?`+resLiCai
    }
    setPartnerBackUrl(linkUrl);
  }
  
  return(
    <div className="pro-detail">
      <audio 
        src={thisSource ? `https://spdb.cdn.bcebos.com/WeeklyMarketInfo/${source}.wav`: null}
        id="audio"
        autoPlay={isAutoPlay}
        ref={audioRef}
        preload="auto"
        onPlay={onAudioPlay}
        onEnded={onAudioEnded}
      />
      {getUrlParam().empId ? <Aicard></Aicard>:null}
      <DisplayCard title={detaildata && detaildata.productName} displaythumb={false} displaybutton={true} {...props} shareClick={shareClickDisplay}>
        {
          detaildata && 
          (getUrlParam().configId === "1" || getUrlParam().configId === "2" ?
            <SlideCard content={detaildata} {...props} isClick={false}>
            </SlideCard>
          :(
             getUrlParam().configId === "3" ?
              <div className="detail-map2">
                <div className="archives-productId" >
                  <div className="divwordcolor">产品代码:{detaildata.productId}</div>
                </div>
                <div className="sub-title">产品档案</div>
                {
                  <div className="archives">
                    {
                      detaildata.archives.map((item,i) => (
                        <div className="archives-item" key={i}>
                          <div className="divwordcolor">{Object.keys(item)}</div>
                          <span className="spanwordcolor">{Object.values(item)}</span>
                        </div>
                      ))
                    }
                  </div> 
                }
                <div className="sub-title">产品要点</div>
                <div className="prodetail-point">
                  {
                    detaildata.point.split(",").map((item,i) => (
                      <div className="tag" key={i}>{item}</div>
                    ))
                  }
                </div>
                
              </div>
            : <div className="detail-map3 archives">
              {
                detaildata.archives.map((item,i) => (
                  <div className="archives-item" key={i}>
                    <div className="divwordcolor">{Object.keys(item)}</div>
                    <span className={`spanwordcolor${Object.keys(item) == '认购时间'? ' red-tag':''}`}>{Object.values(item)}</span>
                  </div>
                ))
              }
                {/* <div className="archives-item" >
                  <div className="divwordcolor">额度</div>
                  <span className="spanwordcolor">{detaildata.quota}</span>
                </div>
                <div className="archives-item" >
                  <div className="divwordcolor">认购时间</div>
                  <span className={"spanwordcolor red-tag"}>{detaildata.archives +'-'+detaildata.ipoEndDate || '-'}</span>
                </div>
                <div className="archives-item">
                  <div className="divwordcolor">年利率</div>
                  <span className="spanwordcolor">{detaildata.rate || '-'} </span>
                </div>
                <div className="archives-item" >
                  <div className="divwordcolor">产品大类</div>
                  <span className="spanwordcolor">{detaildata.productCategory || '-'}</span>
                </div>
                <div className="archives-item">
                  <div className="divwordcolor">产品定位</div>
                  <span className="spanwordcolor">{detaildata.productPosition || '-'}</span>
                </div>
                <div className="archives-item">
                  <div className="divwordcolor">产品类型</div>
                  <span className="spanwordcolor">{detaildata.productType || '-'}</span>
                </div> */}
              </div>
          ))
        }
      </DisplayCard>

      <div className="chat-bubble">
        { chatContent.current.map((content, i) => (
            <div className="chat-bubble-line" key={i}>
            { Object.keys(content) =='left' && Object.values(content) &&
            <div className="entirecard">
              <div className="topcard">
                <div className="share-audio" onClick={() => onPlayAudioClick(i,content.left.tab)}>
                  {/* <img src={require(isPlayVoice && i===curCAudio? `Imgs/audio.gif` : 'Imgs/audio.png')} alt=""/> */}
                  <img src={require(isPlayVoice && i===curCAudio? `Imgs/audio-new.png` : 'Imgs/audio-new.png')} style={{height:'10px',width:'15px'}} alt=""/>
                  <span className={"audio-duration"}>{isPlayVoice && i===curCAudio? "暂停":"播放"}</span>
                </div>
                  <div className="title">
                    
                    {
                      content.left.tab=="推荐理由"?
                      <img src={require('../../public/imgs/recommend-white.png')} style={{width: '15px'}}></img>
                      :
                        content.left.tab=="管理人介绍"?
                        <img src={require('../../public/imgs/person.png')} style={{width: '15px'}}></img>
                        :
                        null
                    }


                    <span className="word">&nbsp;{content.left.tab}</span>
                  </div>
                  <div className="picture2div">
                      <img className="picture2" src={require('Imgs/topcardxiaopu.png')}></img>
                  </div>
              </div>
              <div className="bottomcard" >
                <div className="assets-box">
                  {
                    content.left.tab === "推荐理由" ?
                    <div 
                      className="reason" 
                      dangerouslySetInnerHTML={{__html:content.left.reason}}
                    />
                    :  content.left.tab === "产品档案" ?
                        <div className="archives">
                          {
                            detaildata.archives.map((item,i) => (
                              <div className="archives-item" key={i}>
                                <div className="divwordcolor">{Object.keys(item)}</div>
                                <span className={`spanwordcolor${Object.keys(item) == '认购时间'? ' red-tag':''}`}>{Object.values(item)}</span>
                              </div>
                            ))
                          }
                        </div> 
                    
                    : (
                      content.left.tab === "起息规则" ?
                        <div 
                          className="reason" 
                          dangerouslySetInnerHTML={{__html:content.left.revenueRule}}
                        />
                      : (
                        content.left.tab === "交易规则" ?
                        <div className="archives">
                          {
                            content.left.transRuleArray.map((item,i) => (
                              <div className="archives-item" key={i}>
                                <div className="divwordcolor">{Object.keys(item)}</div>
                                <span className="spanwordcolor">{Object.values(item)}</span>
                              </div>
                            ))
                          }
                        </div>
                        : <div className="managerdata">
                            <div className="mananger-title">{"基金经理"}</div>
                            <div className="managerdataname">{content.left.manager.managerName}</div>
                            <div className="workingyear">{"任职年限：" + content.left.manager.officeTerm}</div>
                            <div className="managerdatadetail">
                              <div className="archives-item">
                                <div className="divwordcolor">目前管理基金数量</div>
                                <span className="spanwordcolor">{content.left.manager.amount}</span>
                              </div>
                              <div className="archives-item" >
                                <div className="divwordcolor">管理规模</div>
                                <span className="spanwordcolor">{content.left.manager.scale}</span>
                              </div>
                              <div className="archives-item">
                                <div className="divwordcolor">基金经理业绩</div>
                                <span className="spanwordcolor red-tag">{content.left.manager.performance|| '-'}</span>
                              </div>
                              <div className="archives-item" >
                                <div className="divwordcolor">同期大盘年化回报</div>
                                <span className="spanwordcolor red-tag">{content.left.manager.payback}</span>
                              </div>
                              <div className="archives-item" >
                                <div className="divwordcolor">规模排名</div>
                                <span className="spanwordcolor">{content.left.manager.rank}</span>
                              </div>
                            </div>
                        </div>
                      )
                    )
                  }
                </div>
              </div>
            </div>
            }
            { Object.keys(content) =='right' && Object.values(content) &&
              <div className="chat-customer">
                <div className="bubble-content">
                    {Object.values(content)}
                </div>
              </div>}
          </div>

        ))}
      </div>

      <div className="tab-list">
        {/* <span className="tab-item" onClick={backPage}>
          返回
        </span> */}
        
        {
          detaildata && detaildata.tabList != undefined ?
            eval(detaildata.tabList).map((item,i) => (
              <span className={getUrlParam().empId?"tab-item-shared":"tab-item"} key={i} onClick={() => send(i,Object.keys(item)[0])}>{Object.keys(item)}</span>
            ))
          :null
        }
        {/* <span>
          <a href="tel:13556891235" className="call-tel" >
            呼叫我
          </a>
        </span> */}
        {
          getUrlParam().configId === "2" &&  getUrlParam().empId?
            <span>
               <a href={"tel:"+tel} className="call-tel-blue" >
                联系理财经理
              </a>
            </span>
          :
            getUrlParam().empId?
            <span className="tab-item-blue purchase-pro">
              <a href={partnerBackUrl} style={{color:"white"}}>点击购买</a>
            </span>
            :null
        }
        
      </div>
      {!getUrlParam().empId ?<Bottomstatisticsbar {...props}></Bottomstatisticsbar>:null}
    </div>
  )
}