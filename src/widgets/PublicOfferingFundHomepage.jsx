import React, {useRef, useState, useEffect} from 'react';
import './PublicOfferingFundHomepage.scss';
import '../../public/config';
import Bottomstatisticsbar from '../components/ui/BottomStatisticsBar/index';
import DisplayCard from '../components/ui/DisplayCard/index';
import SlideCard from '../components/ui/SlideCard/index';
import DailyNewFund from '../components/ui/DailyNewFund/index';
import { getUrlParam, getWeekDay} from 'Utils';
import api from '../axios/api';
import moment from "moment";
import Aicard from '../components/ui/Aicard/index';
import { Toast } from 'antd-mobile';

export default props => {
	const { history, location }=props;
	const [proType, setProType] = useState();

	const dateArr = ['一', '二', '三', '四', '五', '六','日','全部'];

	const chatContent = useRef([]);
	const [chatBubble, setChatBubble] = useState([]);
	const [productInfo, setProductInfo] = useState([]);
	const [curDate, setCurdate] = useState(null);

	useEffect(() => {
		Toast.loading('Loading...', 30,);
		let currentDate = moment().format("YYYY-MM-DD");
		getDayProinfo(currentDate);
		chatContent.current = [];
		setChatBubble([]);
		// getDayProinfo("2021-05-24");
	},[]);

	//每天数据
	const getDayProinfo = (date) => {
		setProType(decodeURIComponent(getUrlParam().proType));
		const params = {
			'mainType':getUrlParam().configId
		}
		api.getProductByType(params).then(res => {
			setProductInfo(res);
			Toast.hide();
		});
	}

	const dateSelect = (i,item) => {
		setCurdate(i);
		let params;
		if(i != dateArr.length-1){
			params = {
				'mainType':getUrlParam().configId,
				'ipoStartDate':getWeekDay(i)
			}
		}else{
			params = {
				'mainType':getUrlParam().configId,
				// 'all':'all'
			}
		}

		api.getProductByType(params).then(res => {
			const obj = {'weekDay':i};
			if(res.length === 0){
				res.push(obj);
			}else{
				Object.assign(res[0],obj)
			}
			chatContent.current = [...chatContent.current, {'right':item},{'left':res}];
			setChatBubble([...chatBubble, {'right':item},{'left':res}]);

		});

	}

	useEffect(()=>{
    let scrollTarget = document.getElementsByClassName("chat-content")[0];
		scrollTarget.scrollTop = scrollTarget.scrollHeight;
	},[chatContent.current.length])

	const shareClick = () => {
		history.push('/canlendar'+location.search+'&curDate='+curDate+'&empId='+ sessionStorage.getItem("employeeId"))
	}

	const shareClickDisplay=  () => {
		history.push(location.pathname + location.search + '&empId='+ sessionStorage.getItem("employeeId"))
	}
	return (
			<div className="publicOfferingFundHomepage">
				{getUrlParam().empId ? <Aicard></Aicard> :null}
				<div className="chat-content">
					<DisplayCard title={proType+ ' 重点产品'} displaythumb={false} displaybutton={true} {...props} shareClick={shareClickDisplay} >
						{
							productInfo.length != 0 ?
							productInfo.map((item,i) => (
								<SlideCard content={item} key={i} {...props} isClick={true}>
								</SlideCard>
							))
							:<div className="date-tip">暂无数据</div>
						}
					</DisplayCard>
						{ chatContent.current.map((content, i) => (
								<div className="chat-bubble" key={i}>
									{ Object.keys(content) =='left' && Object.values(content) &&
										<DailyNewFund curWeekDay={Object.values(content)[0][0].weekDay} proType={decodeURIComponent(getUrlParam().proType)} shareClick={shareClick} {...props}>
											<div className="chat-left">
												{
													Object.values(content)[0][0].ipoStartDate 
													?
														Object.values(content)[0].map((item,i) => (
														 <SlideCard content={item} {...props} key={i} isClick={true}>
														 </SlideCard>
													 
													 ))
													:<div className="date-tip">暂无数据</div>
												}
												
											</div>
										</DailyNewFund>
									}
									{ Object.keys(content) =='right' && Object.values(content) &&
										<div className="chat-right">
											<div className="banner-text">{content.right != '全部新发' ? (content.right=="全部"?content.right:'周'+content.right) : content.right}</div>
										</div>
									}
								</div>
						))}
				</div>
				<div className="whatdaychoice">
					<div className="whatdaychoiceupper">
							{/* <div className="whatdaychoiceupperreturn" onClick={back}>
									<img className="arrow" src={require('Imgs/arrow-back.png')}></img>
									<img className="picture" src={require('Imgs/backtopyramid.png')}></img>
							</div> */}
							<div className="whatdaychoiceupperword">本周每日新发</div>
					</div>
					<div className="whatdaychoicelower">
						{
							dateArr.map((item,i) => (
							<div className="word" key={i} onClick={() => dateSelect(i,item)}>{item}</div>
							))
						}
					</div>
				</div>
				{!getUrlParam().empId ?<Bottomstatisticsbar {...props}></Bottomstatisticsbar>:null}
			</div>
	);
};