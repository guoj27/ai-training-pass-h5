import React, {useState, useEffect} from 'react';
import './WeeklyProductTraining.scss';
import '../../public/config';
import Bottomstatisticsbar from '../components/ui/BottomStatisticsBar/index';
import DisplayCard from '../components/ui/DisplayCard/index';
import Aicard from '../components/ui/Aicard/index';
import { getUrlParam } from 'Utils';
import api from '../axios/api';
import { Toast } from 'antd-mobile';

export default props => {
    const {history, location} =props;
    const [pyramidConfigs, setPyramidConfigs] = useState([]);

    useEffect(() => {
        let weaEmpid=getUrlParam().empId;
        console.log('weaEmpid',weaEmpid)
        if (weaEmpid=='undefined') {
            if (!sessionStorage.getItem('logined')) {
                // onError('请先登录');
                props.history.push('/');
            }
        }
        Toast.loading('Loading...', 30,);
        api.getPyramidConfigs().then(res => {
            setPyramidConfigs(res);
            Toast.hide();
		});
    },[])

    const prodetail = (index,type) => {
        if(location.search){
            history.push(`/publicOffer?configId=`+ index +'&proType='+type +'&empId=' + getUrlParam().empId)
        }else{
            history.push(`/publicOffer?configId=`+ index+'&proType='+type);
        }
    }

    const shareClick = () => {
        history.push(location.pathname + '?empId='+ sessionStorage.getItem("employeeId"))
    }

    return (
        <div className="weeklyproducttraining">
            {getUrlParam().empId?<Aicard></Aicard>:null}
            <DisplayCard title={"每周重点产品"} bottomcardheight={'460px'} displaybutton={true} shareClick={shareClick}  {...props} >
                <div className="pyramidword">本周基金配置</div>
                <img src={require('../../public/imgs/pyramid.png')} className="pyramidpicture"></img>
                {pyramidConfigs.map((item, index) => (
                    <button className={`fund-tip b${index}`} key={index} onClick={ () => prodetail(item.configId,item.productType) }>
                        {index<3?
                        <img className="p1" src={require('../../public/imgs/fundicongray.png')} ></img>
                        :
                        <img className="p1" src={require('../../public/imgs/fundiconblue.png')} ></img>
                        }
                        <span className="word">{item.productType}</span>
                        <img className="p2" src={require('../../public/imgs/fundiconarrow.png')} ></img>
                    </button>
                ))}
                {/* <img className="ll1" src={require('../../public/imgs/leaderline1.png')}></img> */}
                {/* <img className="ll3" src={require('../../public/imgs/leaderline2.png')}></img> */}
            </DisplayCard>
            {!getUrlParam().empId ?<Bottomstatisticsbar></Bottomstatisticsbar>:null}
        </div>
    );
};